LOGO="\n███╗░░░███╗██╗░██████╗░█████╗░██╗░░██╗██╗"
LOGO+="\n████╗░████║██║██╔════╝██╔══██╗██║░██╔╝██║"
LOGO+="\n██╔████╔██║██║╚█████╗░███████║█████═╝░██║"
LOGO+="\n██║╚██╔╝██║██║░╚═══██╗██╔══██║██╔═██╗░██║"
LOGO+="\n██║░╚═╝░██║██║██████╔╝██║░░██║██║░╚██╗██║"
LOGO+="\n╚═╝░░░░░╚═╝╚═╝╚═════╝░╚═╝░░╚═╝╚═╝░░╚═╝╚═╝"
LOGO+="\n🌀 𝐌𝐢𝐬𝐚𝐤𝐢 𝐔𝐬𝐞𝐫𝐁𝐨𝐭 🌀 INSTALLER"
MESAJ="\n🌀 WITH ANDROID"
MESAJ+="\n "
MESAJ+="\n📣 TELEGRAM: @MisakiUserBot"
MESAJ+="\n "
MESAJ+="\n🧩 PLUGIN: @MisakiPlugin"
MESAJ+="\n "
MESAJ+="\n🌀 GEREKSİNİMLER KURULANA DEK TERMUX'TAN AYRILMAYIN!"
MESAJ+="\n "
MESAJ+="\n🌀 DO NOT LEAVE FROM TERMUX UNTIL THE REQUIREMENTS ARE INSTALLING!"
MESAJ+="\n "
YARDIM="\n📌 KURULUM %50, %70 VE %75'TE DURAKLADIĞINDA Y YAZIP ENTERLAYIN!"
YARDIM+="\n "
YARDIM+="\n📌 WRITE Y AND ENTER WHEN THE INSTALLATION IS PAUSED AT 50%, 70% AND 75%!"
YARDIM+="\n "
BOSLUK="\n "
echo -e $LOGO
echo -e $YARDIM
sleep 3
echo "⏳ PAKETLERİ GÜNCELLİYORUM..."
echo "⏳ UPDATING PACKAGES..."
echo -e $BOSLUK
pkg update -y
clear
echo -e $LOGO
echo -e $MESAJ
echo "⌛ PYTHON KURUYORUM..."
echo "⌛ INSTALLING PYTHON..."
echo -e $BOSLUK
pkg install python -y
pip install --upgrade pip
clear
echo -e $LOGO
echo -e $MESAJ
echo "⌛ GIT KURUYORUM..."
echo "⌛ INSTALLING GIT..."
echo -e $BOSLUK
pkg install git -y
clear
echo -e $LOGO
echo -e $MESAJ
echo "⌛ TELETHON KURUYORUM..."
echo "⌛ INSTALLING TELETHON..."
echo -e $BOSLUK
pip install telethon
clear
echo -e $LOGO
echo -e $MESAJ
echo "⌛ 🌀 MİSAKİ INSTALLER İNDİRİYORUM..."
echo "⌛ 🌀 DOWNLOADING MİSAKİ INSTALLER..."
echo -e $BOSLUK
git clone https://github.com/ByMisakiMey/misakiinstaller
clear
echo -e $LOGO
echo -e $MESAJ
echo "⌛ GEREKSİNİMLERİ KURUYORUM..."
echo "⌛ INSTALLING REQUIREMENTS..."
echo -e $BOSLUK
cd misakiinstaller
pip install wheel
pip install -r requirements.txt
python -m installer
